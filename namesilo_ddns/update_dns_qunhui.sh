#!/bin/sh
#这里以 abc.def.xyz 举例
#domain填 def.xyz
domain=
#fulldomain填 abc.def.xyz
fulldomain=
#full填 abc
full=
#namesilo申请的apikey
apikey=
#.sh和.txt路径，是为了保存修改后的ip和每次修改ip的记录
qunhui_home=/volume1/homes/xxx
#获取当前ip
newip=$(curl -s ip.gs)
#获取之前的ip
oldip=$(cat $qunhui_home/ip.txt)
#ip没有变化结束
if [ $newip = $oldip ]; then
 exit
fi
#有变化更新到namesilo
RESPONSE="$(curl -s "https://www.namesilo.com/api/dnsListRecords?version=1&type=xml&key=$apikey&domain=$domain")"
RECORD_ID="$(echo $RESPONSE | sed -n "s/^.*<record_id>\(.*\)<\/record_id>.*<type>A<\/type><host>$fulldomain<\/host>.*$/\1/p")"
RESPONSE="$(curl -s "https://www.namesilo.com/api/dnsUpdateRecord?version=1&type=xml&key=$apikey&domain=$domain&rrid=$RECORD_ID&rrhost=$full&rrvalue=$newip&rrttl=7207")"

echo $RESPONSE
#记录更新时间和ip
now=$(date "+%Y-%m-%d-%H:%M:%S")
echo $now"  ip:"$newip >> $qunhui_home/ip_log.txt
#更新ip.txt
:> $qunhui_home/ip.txt
echo "$newip" >> $qunhui_home/ip.txt

qywx_msg=ip修改为$newip
exec ./qywx_msg.sh $qywx_msg