#!/bin/bash
#要发送的消息
msg=$1
content=$msg
#企业id
qywx_corpid=
#企业应用的secret
qywx_corpsecret=
#指定接收消息的成员
touser=
#指定接收消息的部门
toparty=
#企业应用的id
agentid=1000004
response_token="$(curl -s "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=$qywx_corpid&corpsecret=$qywx_corpsecret")"
other_token="{\"errcode\":0,\"errmsg\":\"ok\",\"access_token\":\""
other_token2="\",\"expires_in\":7200}"
response_token=${response_token/$other_token/}
access_token=${response_token/$other_token2/}
msg_body="{\"touser\":\"$touser\",\"toparty\":\"$toparty\",\"msgtype\":\"text\",\"agentid\":$agentid,\"text\":{\"content\":\"$content\"}}"
req_send_msg_url=https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=$access_token
req_msg=$(curl -s -H "Content-Type: application/json" -X POST -d $msg_body $req_send_msg_url)
echo $req_msg